@extends('shopify-app::layouts.default')

@section('styles')
    @parent
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <style>
        .icon-list .icon-item {
            display: inline-block;
            cursor: pointer;
            width: 38px;
            height: 38px;
            margin: 1px;
            -webkit-box-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            align-items: center;
            border: 2px solid transparent;
            background: #222222;
        }
        .icon-list .icon-item.active {
            border-color: #333;
        }
        .form-zero {
            display: none;
        }
        .form-zero.loaded {
            display: block;
        }
        .loading.loaded {
            display: none;
        }
        .wezu-scroll-top {
            display: block; background-color: #FF9800; width: 50px; height: 50px; text-align: center; border-radius: 4px;
            position: fixed; bottom: 50px; right: 50px; transition: background-color .3s, opacity .5s,
            visibility .5s; z-index: 1000; cursor: pointer; padding: 5px;
        }
        .wezu-scroll-top.bottom-right {
            bottom: 50px;
            right: 50px;
        }
        .wezu-scroll-top.bottom-left {
            bottom: 50px;
            right: auto;
            left: 50px;
        }
        .wezu-scroll-top.center-right {
            bottom: 50%;
        }
        .wezu-scroll-top.center-left {
            bottom: 50%;
            right: auto;
            left: 50px;
        }
        .wezu-icon {position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%);}
    </style>
@endsection

@section('content')
    <div
        x-data="contactForm()"
        style="background-color: rgb(246, 246, 247); color: rgb(32, 34, 35); min-height: 100vh; padding-top: 25px;"
    >
        <div class="container">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <button class="nav-link active" id="setting-tab" data-bs-toggle="tab" data-bs-target="#pills-setting" type="button" role="tab" aria-controls="setting" aria-selected="true">Setting</button>
                </li>
                <li class="nav-item" role="presentation">
                    <button class="nav-link" id="help-tab" data-bs-toggle="tab" data-bs-target="#pills-help" type="button" role="tab" aria-controls="help" aria-selected="false">Help</button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-setting" role="tabpanel" aria-labelledby="pills-setting-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="loading" :class="hasInit && 'loaded'">
                                <div class="spinner-border text-success" role="status">
                                    <span class="visually-hidden">Loading...</span>
                                </div>
                            </div>
                            <div class="form-zero" :class="hasInit && 'loaded'">
                                <div class="alert alert-success" role="alert">
                                    <p>As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront.</p>
                                    <p>
                                        <button class="btn btn-primary btn-sm" onclick="goEdit()">Enable / Disable App</button>
                                        <button class="btn btn-secondary btn-sm" onclick="(new bootstrap.Tab(document.querySelector('#help-tab'))).show()">Learn more</button>
                                    </p>
                                </div>

                                <p x-text="message"></p>
                                <form @submit.prevent="submitData">
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Position</label>
                                        <select id="position" class="form-select" x-model="meta_value.position">
                                            <option value="center-left">Center Left</option>
                                            <option value="center-right">Center Right</option>
                                            <option value="bottom-left">Bottom Left</option>
                                            <option value="bottom-right">Bottom Right</option>
                                        </select>
                                    </div>
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Background</label>
                                        <div class="col-3">
                                            <input class="form-control" type="color" x-model="meta_value.background">
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Icon</label>
                                        <div class="form-control icon-list">
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 1 && 'active'" @click="meta_value.icon = 1"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><polygon :fill="meta_value.icon_color" points="396.6,352 416,331.3 256,160 96,331.3 115.3,352 256,201.5 "></polygon></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 2 && 'active'" @click="meta_value.icon = 2"><svg viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M14.83 30.83l9.17-9.17 9.17 9.17 2.83-2.83-12-12-12 12z"></path><path d="M0 0h48v48h-48z" fill="none"></path></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 3 && 'active'" @click="meta_value.icon = 3"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M413.1,327.3l-1.8-2.1l-136-156.5c-4.6-5.3-11.5-8.6-19.2-8.6c-7.7,0-14.6,3.4-19.2,8.6L101,324.9l-2.3,2.6  C97,330,96,333,96,336.2c0,8.7,7.4,15.8,16.6,15.8v0h286.8v0c9.2,0,16.6-7.1,16.6-15.8C416,332.9,414.9,329.8,413.1,327.3z"></path></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 4 && 'active'" @click="meta_value.icon = 4"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><polygon :fill="meta_value.icon_color" points="1 28 16 4 31 28 16 18 1 28"></polygon></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 5 && 'active'" @click="meta_value.icon = 5"><svg viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg"><g :fill="meta_value.icon_color"><polygon points="0.046,24.418 2.13,26.502 12.967,15.666 23.803,26.502 25.887,24.418 12.967,11.498  "></polygon><polygon points="0.046,13.418 2.13,15.502 12.967,4.666 23.803,15.502 25.887,13.418 12.967,0.498  "></polygon></g></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 6 && 'active'" @click="meta_value.icon = 6"><svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><g><rect fill="none" height="32" width="32"></rect></g><g id="arrow_x5F_full_x5F_up"><polygon :fill="meta_value.icon_color" points="2,16 9.999,16 9.999,30 21.999,30 21.999,16 29.999,16 15.999,2  "></polygon></g></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 7 && 'active'" @click="meta_value.icon = 7"><svg viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M52,84V21.656l21.457,21.456c1.561,1.562,4.095,1.562,5.656,0.001c1.562-1.562,1.562-4.096,0-5.658L50.829,9.172l0,0  c-0.186-0.186-0.391-0.352-0.609-0.498c-0.101-0.067-0.21-0.114-0.315-0.172c-0.124-0.066-0.242-0.142-0.373-0.195  c-0.135-0.057-0.275-0.089-0.415-0.129c-0.111-0.033-0.216-0.076-0.331-0.099C48.527,8.027,48.264,8,48.001,8l0,0  c-0.003,0-0.006,0.001-0.009,0.001c-0.259,0.001-0.519,0.027-0.774,0.078c-0.12,0.024-0.231,0.069-0.349,0.104  c-0.133,0.039-0.268,0.069-0.397,0.123c-0.139,0.058-0.265,0.136-0.396,0.208c-0.098,0.054-0.198,0.097-0.292,0.159  c-0.221,0.146-0.427,0.314-0.614,0.501L16.889,37.456c-1.562,1.562-1.562,4.095-0.001,5.657c1.562,1.562,4.094,1.562,5.658,0  L44,21.657V84c0,2.209,1.791,4,4,4S52,86.209,52,84z"></path></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 8 && 'active'" @click="meta_value.icon = 8"><svg viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M128.4,189.3L233.4,89c5.8-6,13.7-9,22.4-9c8.7,0,16.5,3,22.4,9l105.4,100.3c12.5,11.9,12.5,31.3,0,43.2  c-12.5,11.9-32.7,11.9-45.2,0L288,184.4v217c0,16.9-14.3,30.6-32,30.6c-17.7,0-32-13.7-32-30.6v-217l-50.4,48.2  c-12.5,11.9-32.7,11.9-45.2,0C115.9,220.6,115.9,201.3,128.4,189.3z"></path></svg></div>
                                            <div class="icon-item" :style="{backgroundColor: meta_value.background}" :class="meta_value.icon == 9 && 'active'" @click="meta_value.icon = 9"><svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M12,3.172L5.586,9.586c-0.781,0.781-0.781,2.047,0,2.828s2.047,0.781,2.828,0L10,10.828v7.242c0,1.104,0.895,2,2,2  c1.104,0,2-0.896,2-2v-7.242l1.586,1.586C15.977,12.805,16.488,13,17,13s1.023-0.195,1.414-0.586c0.781-0.781,0.781-2.047,0-2.828  L12,3.172z"></path></svg></div>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <label for="position" class="form-label">Icon Color</label>
                                        <div class="col-3">
                                            <input class="form-control" type="color" x-model="meta_value.icon_color">
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary" x-text="buttonLabel" :disabled="loading">Save</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-help" role="tabpanel" aria-labelledby="pills-help-tab">
                    <div class="card">
                        <div class="card-body">
                            <div class="alert alert-success" role="alert">
                                <p>As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront.</p>
                                <p>
                                    <button class="btn btn-primary btn-sm" onclick="goEdit()">Enable / Disable App</button>
                                    <button class="btn btn-secondary btn-sm" onclick="(new bootstrap.Tab(document.querySelector('#setting-tab'))).show()">Setting</button>
                                </p>
                            </div>
                            <h3>Setting up the app</h3>
                            <h4>Step 1.</h4>
                            <p>Change the icon style you want to apply to the store.</p>
                            <h4>Step 2.</h4>
                            <p>Save the setting by clicking on the 'Save' button.</p>
                            <h4>Step 3.</h4>
                            As we're using the Shopify theme app extension feature, you'll have to enable or disable it from the theme customizer to see the impacts on the storefront. <br />
                            Click on the <strong>"IMZ Scroll To Top"</strong> to enable this app from theme customizer.
                            <h4>Step 4.</h4>
                            <p>After that click on the "Save button"</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wezu-scroll-top" :class="meta_value.position" :style="{ backgroundColor: meta_value.background }">
            <svg x-show="meta_value.icon == 1" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><polygon :fill="meta_value.icon_color" points="396.6,352 416,331.3 256,160 96,331.3 115.3,352 256,201.5 "></polygon></svg>
            <svg x-show="meta_value.icon == 2" viewBox="0 0 48 48" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M14.83 30.83l9.17-9.17 9.17 9.17 2.83-2.83-12-12-12 12z"></path><path d="M0 0h48v48h-48z" fill="none"></path></svg>
            <svg x-show="meta_value.icon == 3" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M413.1,327.3l-1.8-2.1l-136-156.5c-4.6-5.3-11.5-8.6-19.2-8.6c-7.7,0-14.6,3.4-19.2,8.6L101,324.9l-2.3,2.6  C97,330,96,333,96,336.2c0,8.7,7.4,15.8,16.6,15.8v0h286.8v0c9.2,0,16.6-7.1,16.6-15.8C416,332.9,414.9,329.8,413.1,327.3z"></path></svg>
            <svg x-show="meta_value.icon == 4" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><polygon :fill="meta_value.icon_color" points="1 28 16 4 31 28 16 18 1 28"></polygon></svg>
            <svg x-show="meta_value.icon == 5" viewBox="0 0 26 26" xmlns="http://www.w3.org/2000/svg"><g :fill="meta_value.icon_color"><polygon points="0.046,24.418 2.13,26.502 12.967,15.666 23.803,26.502 25.887,24.418 12.967,11.498  "></polygon><polygon points="0.046,13.418 2.13,15.502 12.967,4.666 23.803,15.502 25.887,13.418 12.967,0.498  "></polygon></g></svg>
            <svg x-show="meta_value.icon == 6" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><g><rect fill="none" height="32" width="32"></rect></g><g id="arrow_x5F_full_x5F_up"><polygon :fill="meta_value.icon_color" points="2,16 9.999,16 9.999,30 21.999,30 21.999,16 29.999,16 15.999,2  "></polygon></g></svg>
            <svg x-show="meta_value.icon == 7" viewBox="0 0 96 96" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M52,84V21.656l21.457,21.456c1.561,1.562,4.095,1.562,5.656,0.001c1.562-1.562,1.562-4.096,0-5.658L50.829,9.172l0,0  c-0.186-0.186-0.391-0.352-0.609-0.498c-0.101-0.067-0.21-0.114-0.315-0.172c-0.124-0.066-0.242-0.142-0.373-0.195  c-0.135-0.057-0.275-0.089-0.415-0.129c-0.111-0.033-0.216-0.076-0.331-0.099C48.527,8.027,48.264,8,48.001,8l0,0  c-0.003,0-0.006,0.001-0.009,0.001c-0.259,0.001-0.519,0.027-0.774,0.078c-0.12,0.024-0.231,0.069-0.349,0.104  c-0.133,0.039-0.268,0.069-0.397,0.123c-0.139,0.058-0.265,0.136-0.396,0.208c-0.098,0.054-0.198,0.097-0.292,0.159  c-0.221,0.146-0.427,0.314-0.614,0.501L16.889,37.456c-1.562,1.562-1.562,4.095-0.001,5.657c1.562,1.562,4.094,1.562,5.658,0  L44,21.657V84c0,2.209,1.791,4,4,4S52,86.209,52,84z"></path></svg>
            <svg x-show="meta_value.icon == 8" viewBox="0 0 512 512" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M128.4,189.3L233.4,89c5.8-6,13.7-9,22.4-9c8.7,0,16.5,3,22.4,9l105.4,100.3c12.5,11.9,12.5,31.3,0,43.2  c-12.5,11.9-32.7,11.9-45.2,0L288,184.4v217c0,16.9-14.3,30.6-32,30.6c-17.7,0-32-13.7-32-30.6v-217l-50.4,48.2  c-12.5,11.9-32.7,11.9-45.2,0C115.9,220.6,115.9,201.3,128.4,189.3z"></path></svg>
            <svg x-show="meta_value.icon == 9" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path :fill="meta_value.icon_color" d="M12,3.172L5.586,9.586c-0.781,0.781-0.781,2.047,0,2.828s2.047,0.781,2.828,0L10,10.828v7.242c0,1.104,0.895,2,2,2  c1.104,0,2-0.896,2-2v-7.242l1.586,1.586C15.977,12.805,16.488,13,17,13s1.023-0.195,1.414-0.586c0.781-0.781,0.781-2.047,0-2.828  L12,3.172z"></path></svg>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
            integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        actions.TitleBar.create(app, {title: 'Dashboard'});
    </script>
    <script>
        function goEdit() {
            const redirect = actions.Redirect.create(app);
            redirect.dispatch(
                actions.Redirect.Action.ADMIN_PATH,
                "/themes/current/editor?context=apps&template=index"
            )
        }
        function contactForm() {
            return {
                hasInit: false,
                loading: true,
                buttonLabel: 'Save',
                is_active: false,
                meta_value: {
                    position: 'bottom-right',
                    background: '#FF9800',
                    icon: 3,
                    icon_color: '#333333',
                },
                message: '',
                getInit(loop = 1) {
                    if (loop >= 3) {
                        this.loading = false;
                        return false;
                    }
                    this.loading = true;
                    this.buttonLabel = 'Loading'
                    window.axios.get('/settings').then((res) => {
                        const data = res.data;
                        this.is_active = data.is_active;
                        this.meta_value = data.meta_value;
                        this.message = '';
                        this.hasInit = true;
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    }).catch(() => {
                        setInterval(() => {
                            loop += 1;
                            this.getInit(loop)
                        }, 5000)
                    })
                },
                init() {
                    window.onload = () => {
                        this.getInit(1)
                    }
                },
                submitData() {
                    this.buttonLabel = 'Saving...'
                    this.loading = true;
                    window.axios.post('/settings', {
                        is_active: this.is_active,
                        meta_value: this.meta_value,
                    }).then(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Setting Saved',
                            duration: 5000,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).catch(() => {
                        this.message = ''
                        let Toast = actions.Toast;
                        const toastNotice = Toast.create(app, {
                            message: 'Ooops! Something went wrong!',
                            duration: 5000,
                            isError: true,
                        });
                        toastNotice.dispatch(Toast.Action.SHOW);
                    }).finally(() => {
                        this.loading = false;
                        this.buttonLabel = 'Save'
                    })
                }
            }
        }
    </script>
@endsection
